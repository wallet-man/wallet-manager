import { ConfigService } from "@nestjs/config";

export interface CorsConfig {
  origin: string;
  localHost: string;
}

export interface BootstrapOptions {
  helmet?: boolean;
  cors?: CorsConfig | false;
}

export const serverOptions = async (
  configService: ConfigService,
): Promise<BootstrapOptions> => {
  const useHelmet = configService.get("HELMET");
  const useCors = configService.get<string>("CORS");
  const corsOrigin = configService.get("CORS_ORIGIN");
  const corsLocalHost = configService.get("CORS_LOCALHOST");

  return {
    helmet: useHelmet === "yes",
    cors:
      useCors === "yes"
        ? {
            origin: corsOrigin,
            localHost: corsLocalHost,
          }
        : false,
  };
};
