import { INestApplication, ValidationPipe } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import * as helmet from "helmet";
import { serverOptions } from "../config/server.options";

export const bootstrapServer = async (
  app: INestApplication,
): Promise<INestApplication> => {
  const options = await serverOptions(app.get(ConfigService));

  if (options.helmet) {
    // TODO: Turn contentSecurityPolicy on in production
    app.use(
      helmet({
        contentSecurityPolicy: false,
      }),
    );
  }

  if (options.cors) {
    const corsOriginList = [options.cors.origin];

    if (options.cors.localHost && options.cors.localHost != "") {
      corsOriginList.push(options.cors.localHost);
    }

    app.enableCors({
      origin: corsOriginList,
    });
  }

  app.useGlobalPipes(
    new ValidationPipe({
      skipMissingProperties: true,
    }),
  );

  const config = new DocumentBuilder()
    .setTitle("Wallet Manager")
    .setDescription("Wallet Manager")
    .setVersion("1.0")
    .addTag("wallet-manager")
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup("api", app, document);

  return app;
};
