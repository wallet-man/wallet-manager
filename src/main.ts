import { Logger } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { bootstrapServer } from "./util/bootstrap-server";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await bootstrapServer(app);
  await app.listen(app.get(ConfigService).get("APP_API_PORT"));

  Logger.log(`Wallet Manager Server Listening at ${await app.getUrl()}`);
}

bootstrap();
