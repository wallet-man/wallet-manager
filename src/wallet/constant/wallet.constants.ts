export const TIMEOUT_SUMSUB_REFRESH = 60 * 60 * 4; // in seconds // total 4 hour
export const CHALLENGE_PREFIX = "challenge:";
export const REFRESH_PREFIX = "refresh:";
export const WALLET_PREFIX = "wallet:";
