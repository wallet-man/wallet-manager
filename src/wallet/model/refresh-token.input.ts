import { Length } from "class-validator";

export class RefreshTokenInput {
  @Length(1, 64)
  public refreshToken: string;
}
