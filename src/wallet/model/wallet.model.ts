export enum KycStatus {
  NEW = "NEW",
  PENDING = "PENDING",
  VALIDATED = "VALIDATED",
  REJECTED = "REJECTED",
}

export class WalletModel {
  walletAddress: string;
  signatureId?: string;
  email?: string;
}
