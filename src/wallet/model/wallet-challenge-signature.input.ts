import { Length } from "class-validator";

export class WalletChallengeSignatureInput {
  @Length(1, 64)
  public walletAddress: string;
  @Length(1, 256)
  public walletChallengeSignature: string;
}
