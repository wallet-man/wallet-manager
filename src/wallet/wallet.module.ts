import { HttpModule } from "@nestjs/axios";
import { Module } from "@nestjs/common";
import { WalletController } from "./controller/wallet.controller";
import { WalletService } from "./service/wallet.service";

@Module({
  imports: [HttpModule],
  controllers: [WalletController],
  providers: [WalletService],
  exports: [WalletService],
})
export class WalletModule {}
