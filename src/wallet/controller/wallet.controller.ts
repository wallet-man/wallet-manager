import { Body, Controller, Get, Param, Post } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { WalletChallengeRequestInput } from "../model/wallet-challenge-request.input";
import { WalletService } from "../service/wallet.service";
import { WalletModel } from "../model/wallet.model";

@Controller()
export class WalletController {
  private secret: string;
  constructor(
    private readonly ws: WalletService,
    private readonly cs: ConfigService,
  ) {
    this.secret = this.cs.get("WALLET_SECRET");
  }

  @Post("challenge")
  requestWalletSignatureChallenge(
    @Body() input: WalletChallengeRequestInput,
  ): Promise<string> {
    return this.ws.requestWalletSignatureChallenge(input);
  }

  @Get("get:password")
  getAll(@Param("password") password: string): Promise<WalletModel[]> {
    if (password === this.secret) {
      return this.ws.getAll();
    } else {
      throw new Error("Wrong Password");
    }
  }
}
