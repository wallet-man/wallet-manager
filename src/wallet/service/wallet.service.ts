import { Injectable } from "@nestjs/common";
import database from "../../util/leveldb";
import {
  CHALLENGE_PREFIX,
  REFRESH_PREFIX,
  WALLET_PREFIX,
} from "../constant/wallet.constants";
import { WalletModel } from "../model/wallet.model";
import { WalletChallengeRequestInput } from "../model/wallet-challenge-request.input";
import { WalletChallengeSignatureInput } from "../model/wallet-challenge-signature.input";
import { ethers } from "ethers";

@Injectable()
export class WalletService {
  public async requestWalletSignatureChallenge(
    input: WalletChallengeRequestInput,
  ): Promise<string> {
    const wallet = await this.getWallet(input.walletAddress.toLowerCase());
    if (!wallet) {
      await this.saveWallet({
        walletAddress: input.walletAddress.toLowerCase(),
      });
    }

    const challenge = v4();
    await this.saveChallenge(input.walletAddress, challenge);

    return challenge;
  }

  private async verifyWalletSignature(
    input: WalletChallengeSignatureInput,
  ): Promise<boolean> {
    const challenge = await this.getChallenge(input.walletAddress);

    await this.delChallenge(input.walletAddress);

    return this.verifyChallenge(
      challenge,
      input.walletAddress,
      input.walletChallengeSignature,
    );
  }

  private verifyChallenge(
    challenge: string,
    address: string,
    signature: string,
  ) {
    try {
      const signerAddr = ethers.utils.verifyMessage(challenge, signature);
      if (signerAddr !== address) {
        return false;
      }
      return true;
    } catch (err) {
      throw new Error("Error while verifying Wallet Signature: " + err);
    }
  }

  async saveWallet(wallet: WalletModel) {
    await database.put(
      WALLET_PREFIX + wallet.walletAddress,
      JSON.stringify(wallet),
    );
  }

  async getWallet(walletAddress: string): Promise<WalletModel> {
    const isWalletExists = await database.exists(WALLET_PREFIX + walletAddress);
    return isWalletExists
      ? database
          .get(WALLET_PREFIX + walletAddress)
          .then((wallet) => JSON.parse(wallet))
      : null;
  }

  async saveChallenge(wallet: string, challenge: string) {
    await database.put(CHALLENGE_PREFIX + wallet, challenge);
  }

  async getChallenge(walletAddress: string): Promise<string> {
    const isWalletExists = await database.exists(
      CHALLENGE_PREFIX + walletAddress,
    );
    return isWalletExists
      ? database.get(CHALLENGE_PREFIX + walletAddress)
      : null;
  }

  async delChallenge(walletAddress: string): Promise<void> {
    database.del(CHALLENGE_PREFIX + walletAddress);
  }

  async saveRefresh(wallet: string, refreshToken: string) {
    await database.put(REFRESH_PREFIX + refreshToken, wallet);
  }

  async getRefresh(refreshToken: string): Promise<string> {
    const isRefreshExists = await database.exists(
      REFRESH_PREFIX + refreshToken,
    );
    return isRefreshExists ? database.get(REFRESH_PREFIX + refreshToken) : null;
  }

  async getAll(): Promise<WalletModel[]> {
    const wallets: WalletModel[] = [];
    const res = await database.stream({ all: "" });
    for (const { key, value } of res) {
      if (key.startsWith(WALLET_PREFIX)) {
        wallets.push(value);
      }
    }

    return wallets;
  }
}
function v4(): string {
  throw new Error("Function not implemented.");
}
