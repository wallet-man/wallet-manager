FROM node:lts-alpine
COPY . .
RUN yarn
RUN yarn build
EXPOSE 4000
CMD ["node", "dist/main.js"]